Deploy steps:

Download and install Erlang/OTP: https://github.com/erlang/otp/releases/download/OTP-26.0.2/otp_win64_26.0.2.exe

Download and install RabbitMQ Server: https://github.com/rabbitmq/rabbitmq-server/releases/download/v3.12.2/rabbitmq-server-3.12.2.exe

Clone or download repository

Add .env in repository root with your SERVER_PORT and RABBIT_MQ_PORT

```bash
npm install
```

```bash
npm run build
```

```bash
npm run serve
```
