import {Request, Response} from 'express';
import {sendTask} from '../queue/index.js';

const processRequest = async (req: Request, res: Response) => {
  console.log(`Received request: ${JSON.stringify(req.body)}`);
  await sendTask(req, res);
}

export default processRequest;
