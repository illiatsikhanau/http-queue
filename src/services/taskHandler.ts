import Task from '../types/Task.js';

const processTask = async (task: Task) => {
  // processing imitation
  await new Promise(resolve => setTimeout(resolve, 1000));

  console.log(`Received request: ${JSON.stringify(task.message)} processed successfully!`);
  return task;
}

export default processTask;
