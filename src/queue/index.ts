import amqp, {Channel, Connection} from 'amqplib';
import {v4 as uuidv4} from 'uuid';
import {Request, Response} from 'express';
import Task from '../types/Task.js';
import processTask from '../services/taskHandler.js';

export const RABBIT_MQ_PORT = process.env.RABBIT_MQ_PORT || '5672';
export const TASKS_QUEUE = 'TASKS_QUEUE';
export const RESULT_QUEUE = 'RESULT_QUEUE';

let connection: Connection;
let channel: Channel;

const connectQueue = async () => {
  connection = await amqp.connect(`amqp://localhost:${RABBIT_MQ_PORT}`);
  channel = await connection.createChannel();

  // queue for tasks
  await channel.deleteQueue(TASKS_QUEUE);
  await channel.assertQueue(TASKS_QUEUE);

  // queue for processing results
  await channel.deleteQueue(RESULT_QUEUE);
  await channel.assertQueue(RESULT_QUEUE);

  await channel.consume(TASKS_QUEUE, async (msg) => {
    if (msg) {
      const task = JSON.parse(msg.content.toString());
      const result = await processTask(task);
      channel.sendToQueue(RESULT_QUEUE, Buffer.from(JSON.stringify(result)));
    }
  });
  console.log(`Queue connection created successfully at amqp://localhost:${RABBIT_MQ_PORT}`);
}

export const sendTask = async (req: Request, res: Response) => {
  // unique id for task
  const taskId = uuidv4();
  const task: Task = {id: taskId, message: req.body};
  channel.sendToQueue(TASKS_QUEUE, Buffer.from(JSON.stringify(task)));

  const resultChannel = await connection.createChannel();
  await resultChannel.consume(RESULT_QUEUE, async (msg) => {
    if (msg) {
      const task: Task = JSON.parse(msg.content.toString());
      if (task.id === taskId) {
        res.send(task.message);
        await resultChannel.close();
      }
    }
  });
}

export default connectQueue;
