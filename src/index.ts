import 'log-timestamp';
import express from 'express';
import dotenv from 'dotenv';
import requestRouter from './routes/index.js';
import connectQueue from './queue/index.js';

dotenv.config();
const SERVER_PORT = process.env.SERVER_PORT || '9000';

connectQueue().then(() => {
  const app = express();
  app.use(express.json());
  app.use('', requestRouter);
  app.listen(SERVER_PORT, async () => {
    console.log(`Server running at http://localhost:${SERVER_PORT}`);
  });
});
