export default interface Task {
  id: string
  message: {
    [key: string]: string
  }
}
