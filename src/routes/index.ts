import express from 'express';
import processRequest from '../services/requestHandler.js';

const router = express.Router();

router.use('/', async (req, res) => {
  await processRequest(req, res);
});

export default router;
